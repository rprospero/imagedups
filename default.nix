{ stdenvNoCC ? (import <nixpkgs> {}).stdenvNoCC, lib ? (import <nixpkgs> {}).lib, pkgs ? (import <nixpkgs> {}).pkgs, parallel ? true, gui ? true }:

with lib.lists;

stdenvNoCC.mkDerivation {
  name = "imagedups";
  src = ../imagedups;
  buildInputs = with pkgs; [ phash gcc9 tbb];
}
