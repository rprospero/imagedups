#include <algorithm>
#include <iostream>
#include <iterator>
#include <map>
#include <string>
#include "pHash.h"

using namespace std;

//Get the image hash off a file
ulong64 my_hash(const string p) {
  ulong64 hash = 0;
  ph_dct_imagehash(p.c_str(), hash);
  return hash;
}

int main(int, char**) {
  string new_file;
  map<string, ulong64> file_names;
  while(getline(cin, new_file)) {
    auto new_hash = my_hash(new_file);
    for(auto [old_file, old_hash] : file_names) {
      int result = ph_hamming_distance(old_hash, new_hash);
      cout << result << "\t" << old_file << "\t" << new_file << endl;
    }
    file_names.insert({new_file, new_hash});
  }
}
