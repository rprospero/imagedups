CC=g++
CFLAGS = -std=c++17
LIBS=-lpHash -lpthread

# %.o: %.cpp
#	$(CC) $(CFLAGS) $<

main: imagedups.cpp
	$(CC) $(CFLAGS) $(LIBS) -o main $<
